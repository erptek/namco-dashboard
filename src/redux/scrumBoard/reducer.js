import scrumBoardActions from './actions';

const initialState = {
  boards: {},
  mainboards: {},
  searchText: '',
  labels: {},
  count: '',
  filter_date: '',
  sort: '',
  page: '',
};
export default function scrumBoardReducer(state = initialState, action) {
  switch (action.type) {
    case scrumBoardActions.SET_BOARDS_DATA:
      return { ...state, ...action.payload };

    case scrumBoardActions.SET_CURRENT_BOARD_DATA:
      return { ...state, ...action.payload };

    case scrumBoardActions.SET_SEARCH_TEXT:
      return {
        ...state,
        searchText: action.payload,
      };
    default:
      return state;
  }
}
