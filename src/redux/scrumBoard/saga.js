// saga.js
import { all, takeEvery, put } from 'redux-saga/effects';
import scrumBoardActions from './actions';
import { loadState, saveState } from '@iso/lib/helpers/localStorage';
import axios from 'axios';

// const getScrumBoards = state => state.scrumBoard;
const host = "https://namco.erptek.net";

function* boardsRenderEffectSaga({ payload }) {
  let boards;
  let filter_date;
  let sort;
  let count;
  let page;
  const result = yield getProject(payload)
  if (result.data) {
    if (result.data.result) {
      boards = result.data.result
    }
    else {
      boards = []
    }
  }
  else {
    boards = []
  }
  let epmec;
  let ton1;
  let ton2;
  let status_type;
  boards.map(item => {
    epmec = item.cut_quantity - item.semi_quantity;
    ton1 = item.semi_quantity - item.bottle_neck_qty;
    ton2 = item.bottle_neck_qty - item.qc_pass;
    if (item.rate_productivity <= 50 && item.semi_quantity > item.bottle_neck_qty) {
      item.status = {
        type: "rate",
        message: "Xử lý mí cạp",
        color: '#ff2e2e'
      }
      return item
    }
    if (epmec > 3*item.real_productivity || ton1 > 3*item.real_productivity || ton2 > 3*item.real_productivity) {
      item.status = {
        type: ton1 >= ton2 && ton1 >= epmec ? "ton1" : ton2 >= epmec && ton2 >= ton1 ? "ton2" : "epmec",
        message: "Giải quyết " + (ton1 >= ton2 && ton1 >= epmec ? "tồn 1" : ton2 >= epmec && ton2 >= ton1 ? "tồn 2" : "Epmec"),
        color: '#ff2e2e'
      }
      return item
    }
    
    if ((ton1 < item.real_productivity/2 || ton2 < item.real_productivity/2) && (item.cut_quantity < item.date_target) && epmec == 0) {
      item.status = {
        type: "cut",
        message: "Cắt thêm ngay",
        color: '#efff00',
      }
      return item
    }
    if (item.rate_productivity >= 90) {
      item.status = {
        type: "good",
        message: "Good",
        color: "#00ff37",
      }
      return item
    }
    item.status = {
      type: "none",
      message: "",
      color: "#fff",
    }
  })
  yield put(scrumBoardActions.setBoardsData({ boards, filter_date, sort, count, page }));
}

function* mainBoardsRenderEffectSaga({ payload }) {
  let mainboards;
  let filter_date;
  let sort;
  let count;
  let page;
  const result = yield getProject(payload)
  console.log(result)
  if (result.data) {
    if (result.data.result) {
      mainboards = result.data.result
    }
    else {
      mainboards = []
    }
  }
  else {
    mainboards = []
  }
  // let epmec;
  // let ton1;
  // let ton2;
  // mainboards.map(item => {
  //   epmec = item.cut_quantity - item.semi_quantity;
  //   ton1 = item.semi_quantity - item.bottle_neck_qty;
  //   ton2 = item.bottle_neck_qty - item.qc_pass;
  //   item.epmec = epmec;
  //   item.ton1 = ton1;
  //   item.ton2 = ton2;
  //   if (item.rate_productivity <= 50 && item.semi_quantity > item.bottle_neck_qty) {
  //     item.status = {
  //       type: "rate",
  //       message: "Xử lý mí cạp",
  //       color: '#ff2e2e'
  //     }
  //     return item
  //   }
  //   if (epmec > 3*item.real_productivity || ton1 > 3*item.real_productivity || ton2 > 3*item.real_productivity) {
  //     item.status = {
  //       type: ton1 >= ton2 && ton1 >= epmec ? "ton1" : ton2 >= epmec && ton2 >= ton1 ? "ton2" : "epmec",
  //       message: "Giải quyết " + (ton1 >= ton2 && ton1 >= epmec ? "tồn 1" : ton2 >= epmec && ton2 >= ton1 ? "tồn 2" : "Epmec"),
  //       color: '#ff2e2e'
  //     }
  //     return item
  //   }
    
  //   if ((ton1 < item.real_productivity/2 || ton2 < item.real_productivity/2) && (item.cut_quantity < item.date_target) && epmec == 0) {
  //     item.status = {
  //       type: "cut",
  //       message: "Cắt thêm ngay",
  //       color: '#efff00',
  //     }
  //     return item
  //   }
  //   if (item.rate_productivity >= 90) {
  //     item.status = {
  //       type: "good",
  //       message: "Good",
  //       color: "#00ff37",
  //     }
  //     return item
  //   }
  //   item.status = {
  //     type: "none",
  //     message: "",
  //     color: "#fff",
  //   }
  //   return item
  // })
  yield put(scrumBoardActions.setBoardsData({ mainboards, filter_date, sort, count, page }));
}

function getProject(params) {
  const headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      "Access-Control-Allow-Methods": "DELETE, POST, GET, OPTIONS",
      "Access-Control-Allow-Headers": "X-Requested-With"
    }
  return axios.post(`/production/specificdashboard`, {params}, {
      headers: headers
  })
    .then(result => { return result; })
    .catch(error => { console.error(error); return Promise.reject(error); });
}

// function getMainBoard(params) {
//   const headers = {
//       'Content-Type': 'application/json',
//       'Access-Control-Allow-Origin': '*',
//       "Access-Control-Allow-Methods": "DELETE, POST, GET, OPTIONS",
//       "Access-Control-Allow-Headers": "X-Requested-With"
//     }
//   return axios.post('/production/mainboard', {params}, {
//       headers: headers
//   })
//     .then(result => { return result; })
//     .catch(error => { console.error(error); return Promise.reject(error); });
// }

function getProjectCount(payload) {
  const headers = {
    'Content-Type': 'application/json',
  }
  return axios.post(`${host}/projectcount/mainboard`, {payload}, {
      headers: headers
  })
    .then(result => { return result; })
    .catch(error => { console.error(error); return Promise.reject(error); }); 
}

function* boardRenderEffectSaga({ payload }) {
  const result = yield updateProject(payload)
  if (result.data.status === 200) {
    console.log("Update success")
  }
  else {
    console.log("Update failed")
  }
}

function updateProject(payload) {
  const headers = {
      'Content-Type': 'application/json',
    }
  return axios.post(`${host}/updateproject`, {payload}, {
      headers: headers
  })
    .then(result => { return result; })
    .catch(error => { console.error(error); return Promise.reject(error); });
}

export default function* scrumBoardSaga() {
  yield all([
    takeEvery(scrumBoardActions.LOAD_BOARDS_DATA_SAGA, boardsRenderEffectSaga),
    takeEvery(
      scrumBoardActions.LOAD_CURRENT_BOARD_DATA_SAGA,
      mainBoardsRenderEffectSaga
    ),
  ]);
}
