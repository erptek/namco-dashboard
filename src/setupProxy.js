const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    createProxyMiddleware("/production/specificdashboard", {
      // target: "http://127.0.0.1:8069",
      target: "https://namco.erptek.net/",
      secure: false,
      changeOrigin: true
    })
  );
};