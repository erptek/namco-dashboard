export default {
  siteName: 'Nam&Co',
  siteIcon: 'ion-flash',
  footerText: `Nam&Co @ ${new Date().getFullYear()} Created by ERPTek, Inc`,
  enableAnimatedRoute: false,
  apiUrl: 'http://yoursite.com/api/',
  google: {
    analyticsKey: 'UA-xxxxxxxxx-1',
  },
  dashboard: '/dashboard',
};
