import React from 'react';
import {
  Avatar,
  InfoWrapper,
  ProjectInfo,
  AvatarWrapper,
  PointWrapper,
  TagWrapper,
  RowWrapper,
} from './BoardListCard.style';

import { IconSvg } from '@iso/components/ScrumBoard/IconSvg/IconSvg';
import TwitterIcon from '@iso/assets/images/twitter.svg';

export default function BoardListCard({ item, onDelete, onEdit }) {
  return (
    <tbody>
      <RowWrapper>
        <td className=''>
          {item.department_line_id}
        </td>
        <td className=''>
          {item.product_template_id}
        </td>
        <td className=''>
          {item.product_v_color}
        </td>
        <td className='text-right number'>
          {item.product_qty}
        </td>
        <td className='text-right number'>
          {item.cut_quantity}
        </td>
        <td className='text-right number'>
          {item.semi_special_qty}
        </td>
        <td className='text-right number'>
          {item.special_quantity}
        </td>
        <td className='text-right number'>
          {item.combine_qty}
        </td>
        <td className='text-right number'>
          {item.consumed_qty}
        </td>
        <td className='text-right number'>
          {item.qc_pass}
        </td>
      </RowWrapper>

    </tbody>
  );
}
