import styled from 'styled-components';

export const ProjectInfo = styled.div`
  display: inline-flex;
  align-items: center;
  flex-wrap: wrap;
  h2 {
    a {
      color: #546cb3;
    }
  }
  p {
    flex: 100%
  }
`;

export const AvatarWrapper = styled.div`
  text-align: center;
`

export const RowWrapper = styled.tr`
  td {
    padding-left: 5px!important;
    padding-right: 5px!important;
    padding-top: 10px!important;
    padding-bottom: 10px!important;
    color: #333!important;
    border: 1px solid #333;
    font-size: 16px;
    font-weight: bold;
    @media (min-width: 1750px) {
      font-size: 27px;
    }
  }
  td.number {
    font-size: 18px;
    @media (min-width: 1750px) {
      font-size: 35px;
    }
  }
  .note {
    font-size: 18px;
  }
  .text-center {
    text-align: center;
  }
  .text-right {
    text-align: right;
  }
  .target-date {
    text-align: center;
    font-size: 34px;
    font-weight: bold;
  }
  .prd-qty {
    text-align: center;
    font-size: 32px;
    font-weight: bold;
  }
  .bg-1 {
    background-color: #bdd6ee;
  }
  .bg-2 {
    background-color: #ffe598;
  }
  .bg-3 {
    background-color: #d9e2f3;
  }
`

export const PointWrapper = styled.span`
  background-color: #2ecc71;
  color: #ffffff;
  padding: 5px;
  border-radius: 12px;
  margin-right: 5px;
  font-weight: bold;
`

export const TagWrapper = styled.span`
  background-color: #67a2e6;
  color: #ffffff;
  padding: 5px;
  border-radius: 12px;
  margin: 0 5px;
  font-weight: bold;
`

export const Avatar = styled.img`
  width: 38px;
  height: 38px;
  border-radius: 50%;
  background-color: #00c6e6;
  margin-right: 16px;
  flex-shrink: 0;
`;

export const InfoWrapper = styled.div`
  font-family: 'Roboto';
  font-weight: 500;
  padding-top: 15px;
`;
export const Title = styled.h2`
  font-size: 16px;
  color: #323332;
  margin-bottom: 3px;
`;
export const CreatedAt = styled.p`
  font-size: 13px;
  color: #838b9d;
  margin: 0;
`;

export const MoreActionWrapper = styled.div`
  p {
    cursor: pointer;
    font-size: 14px;
    color: #788195;
    text-transform: capitalize;
    padding: 4px 0;
    font-weight: 500;
    transition: color 0.3s ease-out;
    &:hover {
      color: #323332;
    }
  }
`;
