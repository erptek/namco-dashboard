import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import scrumBoardActions from '@iso/redux/scrumBoard/actions';
import NoBoardFounds from '../BoardNotFound/BoardNotFound';
import BoardListCard from './BoardListCard/BoardListCard';
import AppLayout from '../../AppLayout/AppLayout';
import { filterProjects } from '@iso/lib/helpers/filterProjects';
import { Table } from './BoardList.style';
function BoardLists({
  boards,
  deleteBoardWatcher,
  editBoard,
  history,
  match,
  boardRenderWatcher,
  count,
  filter_date,
  sort,
  page,
}) {
  useEffect(() => {
    boardRenderWatcher({db: "nc_mrp_05", login: "admin", password: "admin", model: "mrp.production", domain: []});
  }, [boardRenderWatcher]);
  useEffect(() => {
    boardRenderWatcher({db: "nc_mrp_05", login: "admin", password: "admin", model: "mrp.production", domain: []});
    const intervalId = setInterval(() => {
      boardRenderWatcher({db: "nc_mrp_05", login: "admin", password: "admin", model: "mrp.production", domain: []});
    }, 1000 * 3) // in milliseconds
    return () => clearInterval(intervalId)
  }, [boardRenderWatcher])

  const handleEdit = board => {
    editBoard(board);
    history.push(`/dashboard/scrum-board/${board.id}`);
  };

  const changeProjectDateFilter = filter => {
    boardRenderWatcher({time_filter: filter, page: 1, type: ['project']});
  };

  const changeProjectSort = sort => {
    boardRenderWatcher({sort: sort, type: ['project']});
  }

  const changPage = page => {
    boardRenderWatcher({page: page, time_filter: filter_date, sort: sort, type: ['project']});
  }

  return (
    <AppLayout changPage={changPage} page={page} filter_date={filter_date} sort={sort} count={count} history={history} match={match} changeProjectDateFilter={changeProjectDateFilter} changeProjectSort={changeProjectSort}>
      {!isEmpty(boards) ? (
        <Table>
          <thead>
            <tr className='title'>
              <th>
                Chuyền
              </th>
              <th>
                Mã hàng
              </th>
              <th>
                Màu
              </th>
              <th>
                Cần làm
              </th>
              <th>
                BTP đã cắt
              </th>
              <th>
                BTP đã nhận
              </th>
              <th>
                Cạp hoàn thành
              </th>
              <th>
                Đã lên xích
              </th>
              <th>
                Đã giật xích
              </th>
              <th>
                QC Pass
              </th>
            </tr>
          </thead>
          {
            !isEmpty(boards) ? (
              boards.map(board => (
                board.special_quantity < board.product_qty ?
                <BoardListCard
                key={board.id ? board.id : (board.department_line_id + board.product_template_id + board.product_v_color)}
                item={board}
              /> : ''
              ))
            ) : <div></div>
          }
        </Table>
      ) : (
        <div></div>
      )}
    </AppLayout>
  );
}

export default connect(
  state => ({
    boards: filterProjects(
      state.scrumBoard.mainboards,
      state.scrumBoard.searchText
    ),
    count: state.scrumBoard.count,
    filter_date: state.scrumBoard.filter_date,
    sort: state.scrumBoard.sort,
    page: state.scrumBoard.page,
  }),
  { ...scrumBoardActions }
)(BoardLists);
