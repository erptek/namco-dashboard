import styled from 'styled-components';

export const Table = styled.table`
  width: 100%;
  th {
    border: 1px solid #333;
    font-size: 18px;
    font-weight: bold;
    @media (min-width: 1750px) {
      font-size: 35px;
    }
  }
  tr.title {
    background-color: #ff891a;
  }
  tr {
    background-color: #f0f6ff;
    td {
      padding: 20px;
      text-align: center;
      @media (min-width: 768px) {
        padding-left: 0;
        padding-right: 0;
        text-align: left;
      }
      color: #788195;
      font-weight: 500;
      .ant-progress {
        > div {
          display: flex;
          align-items: center;
        }
        .ant-progress-text {
          color: #788195;
          font-size: 14px;
          font-weight: 500;
        }
      }
    }
  }
`;
