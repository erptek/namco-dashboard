import React from 'react';
import { connect } from 'react-redux';
import { Layout, Button, Select } from 'antd';
import SearchInput from '@iso/components/ScrumBoard/SearchInput/SearchInput';
import { Scrollbars } from 'react-custom-scrollbars';
import modalActions from '@iso/redux/modal/actions';
import scrumBoardActions from '@iso/redux/scrumBoard/actions';
import { Title, Filters, Header, HeaderSecondary, PageWrapper } from './AppLayout.style';
import {
  LeftOutlined,
  RightOutlined,
} from '@ant-design/icons';

const { Content } = Layout;
const { Option } = Select;

const AppLayout = ({ changPage, page, children, setSearchText, history, match, changeProjectDateFilter, changeProjectSort, count, filter_date, sort }) => (
  <Layout style={{ backgroundColor: '#fff' }}>

    {/* <HeaderSecondary>
      <SearchInput onChange={(value) => setSearchText(value)} />
      <Filters>
        <PageWrapper>{count > 0 ? ((page - 1)*20+1)+'-'+(page*20 < count ? page*20 : count) : '0-0'}/{count}</PageWrapper>
        <Button
          disabled={(page === 1 || count === 0) ? true : false}
          onClick={(event) => {changPage(page-1)}}
        >
          <LeftOutlined />
        </Button>
        <Button
          disabled={page*20 >= count ? true : false}
          onClick={(event) => {changPage(page+1)}}

        >
          <RightOutlined />
        </Button>
      </Filters>
    </HeaderSecondary> */}

    <Content
      style={{
        padding: '0 24px',
      }}
    >
      <Scrollbars style={{ width: '100%', height: 'calc(100vh - 80px)' }}>
        {children}
      </Scrollbars>
    </Content>
  </Layout>
);

export default connect(null, { ...modalActions, ...scrumBoardActions })(
  AppLayout
);
