import React from 'react'
import {
  Avatar,
  InfoWrapper,
  ProjectInfo,
  AvatarWrapper,
  PointWrapper,
  RowWrapper,
} from './ProjectList.style';

import { IconSvg } from '@iso/components/ScrumBoard/IconSvg/IconSvg';
import TwitterIcon from '@iso/assets/images/twitter.svg';
import ContentHolder from '@iso/components/utility/contentHolder';
import Select, { SelectOption } from '@iso/components/uielements/select';
const Option = SelectOption;

const tag_options = ["Ethereum","NFTs","Loot","DAOs","Stablecoins","Matic/Polygon"]
const children = [];
tag_options.map(tag => {
    children.push(<Option key={tag}>{tag}</Option>);
})

export default function ProjectList({ item, onDelete, onEdit, onUpdateTag, onUpdateType }) {
  
  return (
    <tbody>
      <RowWrapper>
        <td className='bg-3'>
          Giờ đã sản xuất {item[0] ? item[0].druation_time : ''}
        </td>
        <td className='bg-1'>
          {item[0] ? item[0].line_name : ''}
        </td>
        <td className="point-col bg-1">
          mã: {item[0] ? item[0].product : ''}
        </td>
        <td className='bg-2'>
          {item[1] ? item[1].line_name : ''}
        </td>
        <td className="point-col bg-2">
          mã: {item[1] ? item[1].product : ''}
        </td>
      </RowWrapper>
      <RowWrapper>
        <td>
          Target ngày
        </td>
        <td colSpan={2} className='target-date bg-1'>
          {item[0] ? item[0].date_target : ''}
        </td>
        <td colSpan={2} className='target-date bg-2'>
          {item[1] ? item[1].date_target : ''}
        </td>
      </RowWrapper>
      <RowWrapper>
        <td>
          Năng suất thực/giờ
        </td>
        <td className='prd-qty bg-1'>
          {item[0] ? item[0].real_productivity : ''}
        </td>
        <td className='prd-qty bg-1'>
          {item[0] ? item[0].rate_productivity : ''}%
        </td>
        <td className='prd-qty bg-2'>
          {item[1] ? item[1].real_productivity : ''}
        </td>
        <td className='prd-qty bg-2'>
          {item[1] ? item[1].rate_productivity : ''}%
        </td>
      </RowWrapper>
      <RowWrapper>
        <td>
          Lao động
        </td>
        <td colSpan={2} className='text-center bg-1'>
          {item[0] ? item[0].worker_count : ''}
        </td>
        <td colSpan={2} className='text-center bg-2'>
          {item[1] ? item[1].worker_count : ''}
        </td>
      </RowWrapper>
      <RowWrapper>
        <td>
        </td>
        <td className='text-center bg-1'>
          Sản lượng
        </td>
        <td className='text-center bg-1'>
          Thời gian nhập
        </td>
        <td className='text-center bg-2'>
          Sản lượng
        </td>
        <td className='text-center bg-2'>
          Thời gian nhập
        </td>
      </RowWrapper>
      <RowWrapper>
        <td>
          QC Pass
        </td>
        <td className='text-center bg-1'>
          {item[0] ? item[0].qc_pass : ''}
        </td>
        <td className='text-center bg-1'>
          {item[0] ? item[0].qc_update_time : ''}
        </td >
        <td className='text-center bg-2'>
          {item[1] ? item[1].qc_pass : ''}
        </td>
        <td className='text-center bg-2'>
          {item[1] ? item[1].qc_update_time : ''}
        </td>
      </RowWrapper>
      <RowWrapper>
        <td>
          Sản lượng công đoạn thắt cổ chai
        </td>
        <td className='text-center bg-1'>
          {item[0] ? item[0].bottle_neck_qty : ''}
        </td>
        <td className='text-center bg-1'>
          {item[0] ? item[0].bottle_update_time : ''}
        </td>
        <td className='text-center bg-2'>
          {item[1] ? item[1].bottle_neck_qty : ''}
        </td>
        <td className='text-center bg-2'>
          {item[1] ? item[1].bottle_update_time : ''}
        </td>
      </RowWrapper>
      <RowWrapper>
        <td>
          Bán thành phẩm đã nhận
        </td>
        <td className='text-center bg-1'>
          {item[0] ? item[0].semi_quantity : ''}
        </td>
        <td className='text-center bg-1'>
          {item[0] ? item[0].semi_update_time : ''}
        </td>
        <td className='text-center bg-2'>
          {item[1] ? item[1].semi_quantity : ''}
        </td>
        <td className='text-center bg-2'>
          {item[1] ? item[1].semi_update_time : ''}
        </td>
      </RowWrapper>
      <RowWrapper>
        <td>
          Đã cắt
        </td>
        <td className='text-center bg-1'>
          {item[0] ? item[0].cut_quantity : ''}
        </td>
        <td className='text-center bg-1'>
          {item[0] ? item[0].cut_update_time : ''}
        </td>
        <td className='text-center bg-2'>
          {item[1] ? item[1].cut_quantity : ''}
        </td>
        <td className='text-center bg-2'>
          {item[1] ? item[1].cut_update_time : ''}
        </td>
      </RowWrapper>
      <RowWrapper>
        <td>
          Chú ý
        </td>
        <td style={{backgroundColor: item[0].status.color}} colSpan={2} className='note'>
          {item[0] ? item[0].status.message : ''}
        </td>
        <td style={{backgroundColor: item[1].status.color}} colSpan={2} className='note'>
          {item[1] ? item[1].status.message : ''}
        </td>
      </RowWrapper>

    </tbody>
  );
}
