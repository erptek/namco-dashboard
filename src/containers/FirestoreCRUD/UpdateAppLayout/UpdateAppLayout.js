import React from 'react';
import { connect } from 'react-redux';
import { Layout, Button, Select } from 'antd';
import SearchInput from '@iso/components/ScrumBoard/SearchInput/SearchInput';
import { Scrollbars } from 'react-custom-scrollbars';
import modalActions from '@iso/redux/modal/actions';
import scrumBoardActions from '@iso/redux/scrumBoard/actions';
import { Filters, HeaderSecondary, PageWrapper } from './UpdateAppLayout.style';
import {
  LeftOutlined,
  RightOutlined,
} from '@ant-design/icons';

const { Content } = Layout;
const { Option } = Select;

const UpdateAppLayout = ({ changPage, page, children, setSearchText, history, match, changeProjectDateFilter, changeProjectSort, count, filter_date, sort }) => (
  <Layout style={{ backgroundColor: '#fff' }}>

    <Content
      style={{
        padding: '0 24px',
      }}
    >
      <Scrollbars style={{ width: '100%', height: 'calc(100vh - 80px)' }}>
        {children}
      </Scrollbars>
    </Content>
  </Layout>
);

export default connect(null, { ...modalActions, ...scrumBoardActions })(
  UpdateAppLayout
);
