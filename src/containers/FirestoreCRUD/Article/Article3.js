import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import scrumBoardActions from '@iso/redux/scrumBoard/actions';
import BoardListCard from '../ProjectList/ProjectList';
import AppLayout from '../UpdateAppLayout/UpdateAppLayout';
import { filterProjects } from '@iso/lib/helpers/filterProjects';
import { Table } from './Article.styles';
function Article3({
  boards,
  deleteBoardWatcher,
  editBoard,
  history,
  match,
  boardsRenderWatcher,
  count,
  filter_date,
  sort,
  page,
}) {
  // useEffect(() => {
  //   boardsRenderWatcher({db: "namco_mrp", login: "admin", password: "admin", model: "mrp.production", domain: ["Line 1", "Line 2"]});
  // }, [boardsRenderWatcher]);
  useEffect(() => {
    boardsRenderWatcher({db: "namco_mrp", login: "admin", password: "admin", model: "mrp.production", domain: ["Line 5", "Line 6"]});
    const intervalId = setInterval(() => {
      boardsRenderWatcher({db: "namco_mrp", login: "admin", password: "admin", model: "mrp.production", domain: ["Line 5", "Line 6"]});
    }, 1000 * 3) // in milliseconds
    return () => clearInterval(intervalId)
  }, [boardsRenderWatcher])

  const handleEdit = board => {
    editBoard(board);
    history.push(`/dashboard/scrum-board/${board.id}`);
  };

  const changeProjectDateFilter = filter => {
    boardsRenderWatcher({time_filter: filter, page: 1, type: ['project']});
  };

  const changeProjectSort = sort => {
    boardsRenderWatcher({sort: sort, type: ['project']});
  }

  const changPage = page => {
    boardsRenderWatcher({page: page, time_filter: filter_date, sort: sort, type: ['project']});
  }

  return (
    <AppLayout changPage={changPage} page={page} filter_date={filter_date} sort={sort} count={count} history={history} match={match} changeProjectDateFilter={changeProjectDateFilter} changeProjectSort={changeProjectSort}>
      {!isEmpty(boards) ? (
        <Table>
            <BoardListCard
              item={boards}
            />
        </Table>
      ) : (
        <div></div>
      )}
    </AppLayout>
  );
}

export default connect(
  state => ({
    boards: filterProjects(
      state.scrumBoard.boards,
      state.scrumBoard.searchText
    ),
    count: state.scrumBoard.count,
    filter_date: state.scrumBoard.filter_date,
    sort: state.scrumBoard.sort,
    page: state.scrumBoard.page,
  }),
  { ...scrumBoardActions }
)(Article3);
